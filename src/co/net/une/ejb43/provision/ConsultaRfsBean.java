package co.net.une.ejb43.provision;

import com.gesmallworld.gss.lib.request.Request;
import com.gesmallworld.gss.lib.request.Response;
import com.gesmallworld.gss.lib.service.magik.MagikService;
import com.gesmallworld.gss.lib.service.magik.MagikService.StateHandling;
import javax.ejb.SessionBean;

/**
 * Service proxy class generated by GSS Code Generator.
 * @ejb.resource-ref
 *     res-ref-name="eis/SmallworldServer"
 *     res-auth="Container"
 *     res-sharing-scope="Shareable"
 *     res-type="javax.resource.cci.ConnectionFactory"
 *     jndi-name="eis/SmallworldServer"
 * @ejb.interface
 *     package="co.net.une.ejb43.interfaces"
 *     local-extends="javax.ejb.EJBLocalObject, com.gesmallworld.gss.lib.service.ChainableServiceLocal"
 * @ejb.bean
 *     local-jndi-name="ejb/ConsultaRfsLocal"
 *     name="ConsultaRfs"
 *     type="Stateless"
 *     transaction-type="Bean"
 *     view-type="local"
 * @ejb.home
 *     package="co.net.une.ejb43.interfaces"
 *     local-extends="javax.ejb.EJBLocalHome"
 */
@StateHandling(serviceProvider = "consulta_rfs_service_provider")
public class ConsultaRfsBean extends MagikService implements SessionBean{

    /**
     * Local jndi name in use for this service bean.
     */
    public static final String LOCAL_JNDI_NAME = "ejb/ConsultaRfsLocal";

    /**
     * Serialisation ID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     */
    public ConsultaRfsBean () {
        super();
    }

    /**
     * Generated service proxy method. Corresponds to a service of a Magik Service Provider.
     * @ejb.interface-method
     * @param request A request object as specified in the service description file.
     * @return A response instance
     */
    @EISMapping(value = "consultar_rfs")
    public Response consultarRfs (Request request) {
        return this.handleInterfaceMethod(request);
    }

    }
